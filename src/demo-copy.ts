// type DataType = 'number' | 'string' | 'object'
//
// interface DataCell {
//     required: boolean
//     dataType: DataType
// }
//
// interface GenericNumberType extends DataCell {
//     dataType: 'number'
// }
//
// interface RangedNumberType extends GenericNumberType {
//     type: 'range'
//     min?: number
//     max?: number
// }
//
// interface PercentageNumberType extends GenericNumberType {
//     type: 'percent'
//     min: 0
//     max: 100
// }
//
// type AllNumberTypes = GenericNumberType | RangedNumberType | PercentageNumberType
//
// interface GenericStringType extends DataCell {
//     dataType: 'string'
// }
//
// interface EnumStringType extends GenericStringType {
//     type: 'enum'
//     validValues: readonly string[]
// }
//
// interface EmailStringType extends GenericStringType {
//     type: 'email',
// }
//
// type AllStringTypes = GenericStringType | EnumStringType | EmailStringType
//
// type AllDataType = AllNumberTypes | AllStringTypes | ObjectType
//
// interface ObjectType extends DataCell {
//     dataType: 'object'
//     blueprint: Entity
// }
//
// type Entity = Record<string, AllDataType>
//
//
// const UserConfig = {
//     name: {
//         dataType: 'string',
//         required: true,
//     },
//     age: {
//         dataType: 'number',
//         required: true,
//     },
//     address: {
//         dataType: 'object',
//         required: false,
//         blueprint: {
//             email: {
//                 dataType: 'string',
//                 required: true,
//                 type: 'email'
//             }
//         }
//     }
// } as const
//
// type GetRequiredKeys<T extends Entity> = {
//     [Prop in keyof T]: T[Prop] extends { required: infer Required } ?
//         Required extends true ?
//             Prop :
//             never
//         : never
// }[keyof T]
//
// type GetOptionalKeys<T extends Entity> = {
//     [Prop in keyof T]: T[Prop] extends { required: infer Required } ?
//         Required extends false ?
//             Prop :
//             never
//         : never
// }[keyof T]
//
//
// type GetType<T extends Entity> = {
//     [Prop in GetRequiredKeys<T>]: T[Prop] extends AllNumberTypes ?
//         number :
//         T[Prop] extends AllStringTypes ?
//             T[Prop] extends EnumStringType ?
//                 T[Prop]['validValues'][number] :
//                 T[Prop] extends EmailStringType ?
//                     `${string}@${string}.${string}` :
//                     string :
//             T[Prop] extends ObjectType ?
//                 GetType<T[Prop]['blueprint']> :
//                 never
// } & {
//     [Prop in GetOptionalKeys<T>]?: T[Prop] extends AllNumberTypes ?
//         number :
//         T[Prop] extends AllStringTypes ?
//             T[Prop] extends EnumStringType ?
//                 T[Prop]['validValues'][number] :
//                 T[Prop] extends EmailStringType ?
//                     `${string}@${string}.${string}` :
//                     string :
//             T[Prop] extends ObjectType ?
//                 GetType<T[Prop]['blueprint']> :
//                 never
// }
//
// type User = GetType<typeof UserConfig>
//
// type Username = User['name']
//
// const username: Username = 15